#!/bin/sh
pyagent run -c appdynamics.conf -- uvicorn --timeout-keep-alive 120 --workers 8 --host 0.0.0.0 --port 8003 app:app