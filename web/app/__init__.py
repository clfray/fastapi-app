from fastapi import FastAPI
import requests
import httpx
import json
import time
import os
import socket



app = FastAPI()

# env_dict = {
#     "APPD_NODE_NAME": os.getenv('APPD_NODE_NAME', 'fastapi-web') + "_" + socket.gethostname()
# }

# appd.init(environ=env_dict, timeout_ms=appd.api.NO_TIMEOUT)

@app.get("/api")
async def root():
    return {"message": "Hello World"}

@app.get("/api/async/users")
async def async_users():
    data = []
    # async with httpx.AsyncClient(app=app, base_url="https://jsonplaceholder.typicode.com") as client:
    async with httpx.AsyncClient(app=app) as client:
        response = await client.get("https://jsonplaceholder.typicode.com/users")
    
    data = json.dumps(response.json())
    print(data)
    return {"status": 200, "data": data}

@app.get("/api/sync/users")
async def sync_users():
    data = []
    response = requests.get(url='https://jsonplaceholder.typicode.com/users')
    data = json.dumps(response.json())

    return {"status": 200, "data": data}



@app.get("/api/sync/posts")
async def sync_posts():
    data = []
    try:
        response = requests.get(url='https://jsonplaceholder.typicode.com/posts')
        data = json.dumps(response.json())
    except Exception as exc:
        raise 'There was an issue'

    return {"status": 200, "data": data}
