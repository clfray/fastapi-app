from locust import HttpUser, task, between

class QuickstartUser(HttpUser):
    wait_time = between(1, 2)
    host = "http://web:8003"
    # def on_start(self):
    #     self.client.post("/login", json={"username":"foo", "password":"bar"})

    @task
    def api_items(self):
        self.client.get("/api/async/users")

    @task
    def api_sync_users(self):
        self.client.get("/api/sync/users")

    @task
    def api_sync_posts(self):
        self.client.get("/api/sync/posts")